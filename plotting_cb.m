
clearvars ; close all; clc

feat = "central bias";

data = load("results/for_anova/cb_matrix_neu.mat");
%extract data
cb_0 = data.anova_matrix;

% calculate means
cbm_0 = mean(cb_0)
% calculate sd
std_0 = std(cb_0); 
%calc SEM
semc_0 = std(cb_0)/sqrt(160);

fig = figure('Renderer', 'painters', 'Position', [10 10 1000 600]);

%Abstand in x Richtung, für bessere Unterschiedung der Datenpunkte 
xDist =  [-0.075 -0.025 0.025 0.075];
x = [1 2 3 4 5];

%plot data
d1 = plot(cbm_0, '.k');hold on;     
set(d1, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(1)));  
set(d1,'MarkerSize', 15)





%plot errorbars
e1 = errorbar(cbm_0, semc_0, '.k');
set(e1, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(1)))           %X-Position ist die gleiche wie für die Daten
set(e1,'LineWidth', 1)                                      %Liniendicke
e1.Annotation.LegendInformation.IconDisplayStyle = 'off';   %Error in Legende ausschließen


xlabel("Klicks");
xticks([1 2 3 4 5]);
ylabel("Pixel to center");
title(feat);

grid on

%Legende erstellen und in oberer linker Ecke platzieren
%legend([d1], {'Standard'},'Location','NorthEastOutside','FontSize', 13) 

saveas(fig, 'figures/cb');

clearvars;

