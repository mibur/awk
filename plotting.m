
clearvars ; close all; clc

feat = "Object Maps";

data = load("results/for_anova/obj.mat");
basel = load("results/for_anova/obj_baseline.mat");
%extract data
bl_0 = basel.bl_0; bl_45 = basel.bl_45; bl_180 = basel.bl_180; bl_45m = basel.bl_45m;
%cb_0 = data.cb_0; cb_45 = data.cb_45; cb_180 = data.cb_180; cb_45m = data.cb_45m;
or_0 = data.or_0; or_45 = data.or_45; or_180 = data.or_180; or_45m = data.or_45m;

% calculate means
mean_0 = mean(or_0); mean_180 = mean(or_180); mean_45 = mean(or_45); mean_45m = mean(or_45m);
%cbm_0 = mean(cb_0); cbm_45 = mean(cb_45); cbm_180 = mean(cb_180); cbm_45m = mean(cb_45m);
% calculate sd
std_0 = std(or_0); std_180 = std(or_180); std_45 = std(or_45); std_45m = std(or_45m);

%calc SEM
sem_0 = std(or_0)/sqrt(160); sem_180 = std(or_180)/sqrt(160); sem_45 = std(or_45)/sqrt(160); sem_45m = std(or_45m)/sqrt(160);
%semc_0 = std(cb_0)/sqrt(160); semc_180 = std(cb_180)/sqrt(160); semc_45 = std(cb_45)/sqrt(160); semc_45m = std(cb_45m)/sqrt(160);

fig = figure('Renderer', 'painters', 'Position', [10 10 1000 600]);

%Abstand in x Richtung, für bessere Unterschiedung der Datenpunkte 
xDist =  [-0.075 -0.025 0.025 0.075];
x = [1 2 3 4 5];

%plot data
d1 = plot(mean_0, '.k');hold on;     
set(d1, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(1)));  
set(d1,'MarkerSize', 15)

d2 = plot(mean_180, '.b'); %'.g' -> Farbe Grün
set(d2, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(2)));
set(d2,'MarkerSize', 15)

d3 = plot(mean_45, '.r');                                  %'.g' -> Farbe Grün
set(d3, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(3)));
set(d3,'MarkerSize', 15)

d4 = plot(mean_45m, '.g');                                  %'.g' -> Farbe Grün
set(d4, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(4)));
set(d4,'MarkerSize', 15)



%plot baseline
b1 = plot(bl_0, '*k');hold on;
set(b1, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(1)));  
set(b1,'MarkerSize', 12)

b2 = plot(bl_180, '*b');                                  %'.g' -> Farbe Grün
set(b2, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(2)));
set(b2,'MarkerSize', 12)

b3 = plot(bl_45, '*r');                                  %'.g' -> Farbe Grün
set(b3, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(3)));
set(b3,'MarkerSize', 12)

b4 = plot(bl_45m, '*g');                                  %'.g' -> Farbe Grün
set(b4, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(4)));
set(b4,'MarkerSize', 12)



%plot errorbars
e1 = errorbar(mean_0, sem_0, '.k');
set(e1, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(1)))           %X-Position ist die gleiche wie für die Daten
set(e1,'LineWidth', 1)                                      %Liniendicke
e1.Annotation.LegendInformation.IconDisplayStyle = 'off';   %Error in Legende ausschließen

e2 = errorbar(mean_180, sem_180, '.b');
set(e2, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(2)))           %X-Position ist die gleiche wie für die Daten
set(e2,'LineWidth', 1)                                      %Liniendicke
e2.Annotation.LegendInformation.IconDisplayStyle = 'off';   %Error in Legende ausschließen

e3 = errorbar(mean_45, sem_45, '.r');
set(e3, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(3)))           %X-Position ist die gleiche wie für die Daten
set(e3,'LineWidth', 1)                                      %Liniendicke
e3.Annotation.LegendInformation.IconDisplayStyle = 'off';   %Error in Legende ausschließen

e4 = errorbar(mean_45m, sem_45m, '.g');
set(e4, 'XData',[1 2 3 4 5]+ones(1,5)*(xDist(4)))           %X-Position ist die gleiche wie für die Daten
set(e4,'LineWidth', 1)                                      %Liniendicke
e4.Annotation.LegendInformation.IconDisplayStyle = 'off';   %Error in Legende ausschließen


xlabel("Klicks", 'FontSize', 20);
xticks([1 2 3 4 5]);
ylabel("NSS", 'FontSize', 20);
title(feat, 'FontSize', 20);

grid on

%Legende erstellen und in oberer linker Ecke platzieren
legend([d1 d2 d3 d4], {'0°','180°', '45°', '225°'},'Location','NorthEastOutside','FontSize', 20) 

clearvars d1 d2 d3 d4 b1 b2 b3 b4 e1 e2 e3 e4 x

