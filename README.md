@author: Micha Burkhardt

There are two versions of the experiment:
1. experiment_psychtoolbox: Built with psychtoolbox
2. experiment_figures: Only uses MATLAB figures

At the top of the script, you need to specify your subject numer, the total number of pictures as well as your screen number 
(for the psychtoolbox one, since automatically setting it up will break on some multi-monitor settings - it will probably be 1 or 2 for you).

@author: Julius Riegger

evaluate.m:
- feature needs to be filled in line 4 and 46
- sorts result-data of subjects to orientations + klick arrays (shape:160 x 5)
    -> compare klick-position with matrix-coords of feature-map and return value to array

baseline.m:
- get baseline of feature-map for each orientation
    -> get mean over every klick from every image exept the image the baseline is  calculated for

result-files:
- Luminance.mat
- Texture.mat
- objMaps.mat

plotting.m: 
- feature needs to be filled at top of script
- for plotting central bias -> change "mean" to "cbm" in line 25,29,33,37
