data = load('results/for_anova/luminance_matrix.mat');
[p,tbl] = anova2(data.anova_matrix,160);

figure()
[~,~,stats] = anova2(data.anova_matrix,160,'off');
mc = multcompare(stats)

