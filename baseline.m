clearvars ; close all; clc
% get baseline array
bl_0raw   = zeros(6240,5); bl_180raw = zeros(6240,5); bl_45raw = zeros(6240,5); bl_45mraw = zeros(6240,5);
cnt1 = 0; cnt2 = 0; cnt3 = 0; cnt4 = 0;
%iterate over subjects
for sub = 1:4
    subject = sprintf('results/results%d.mat', sub);
    load(subject);
    for i=100:numel(numbers)+99
        nr = numbers(i-99);
        %select feature 
        file = sprintf("Merkmalskarten/T_C/%d.mat", nr);
        tmp = load(file);
        featmap = tmp.featMap;
        
        m_featmap = mean(featmap(:));
        std_featmap = std(featmap(:));
        
        sfm = size(featmap);
        for s1=1:sfm(1)
            for s2=1:sfm(2)
                featmap(s1,s2) = (featmap(s1,s2) - m_featmap) / std_featmap;
            end
        end
        
        %get orientation of image 
        orientation = orientations(i-99); % 99, because img-no starts at 100
        %iterate over other imageresults
        for j = 1:numel(numbers)
            %jump over iteration, when its the same as the one the baseline is
            %calculated for
            if i-99 == j
                continue
            end

            %only add klicks, if orientations are equal
            orientation_comp = orientations(j);
            if orientation == orientation_comp
                %switch between counter variables, 
                switch orientation
                        case 1 
                            cnt1 = cnt1 + 1;
                        case 2
                            cnt2 = cnt2 + 1;
                        case 3
                            cnt3 = cnt3 + 1;
                        case 4
                            cnt4 = cnt4 + 1;
                end
                
                %iterate over klick number
                for k = 1:5
                    x = results(j,k);
                    y = results(j,k+5);
                    
                    % continue, if y or x value is out of range of scaned
                    % feature map
                    [ny, nx] = size(featmap);
                    if y > ny  || x > nx 
                        continue
                    end
                    switch orientation % add klicks to array
                        case 1 % 0 Grad
                            bl_0raw(cnt1,k) = featmap(y,x);
                        case 2 % -45 Grad
                            bl_45mraw(cnt2,k) = featmap(y,x);
                        case 3 % 180 Grad
                            bl_180raw(cnt3,k) = featmap(y,x);
                        case 4 % 45 Grad
                            bl_45raw(cnt4,k) = featmap(y,x);
                    end
                end
            end
        end
    end
end

clearvars i j k ny nx sub x y tmp subject cnt1 cnt2 cnt3 cnt4 orientation orientation_comp nr iamge_size screen_size

%calculate baseline by mean
bl_0 = mean(bl_0raw); 
bl_180 = mean(bl_180raw); 
bl_45 = mean(bl_45raw); 
bl_45m = mean(bl_45mraw);

save('results/for_anova/tc_baseline.mat', 'bl_0','bl_45','bl_180','bl_45m');
