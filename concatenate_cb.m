data1 = load("results/for_anova/cb_matrix1.mat");
data2 = load("results/for_anova/cb_matrix2.mat");
data3 = load("results/for_anova/cb_matrix3.mat");

data1 = data1.anova_matrix;
data2 = data2.anova_matrix;
data3 = data3.anova_matrix;

new = zeros(1920,5);

new(1:160,:) = data1(1:160,:);
new(161:320,:) = data2(1:160,:);
new(321:480,:) = data3(1:160,:);

new(481:640,:) = data1(161:320,:);
new(641:800,:) = data2(161:320,:);
new(801:960,:) = data3(161:320,:);

new(961:1120,:) = data1(321:480,:);
new(1121:1280,:) = data2(321:480,:);
new(1281:1440,:) = data3(321:480,:);

new(1441:1600,:) = data1(481:640,:);
new(1601:1760,:) = data2(481:640,:);
new(1761:1920,:) = data3(481:640,:);


save('results/for_anova/cb_matrix.mat', 'new');
writematrix(new,'results/for_anova/cb_matrix.txt','Delimiter','\t');


new1 = zeros(480,5);
new2 = zeros(480,5);
new2 = zeros(480,5);
new4 = zeros(480,5);

new1(1:160,:) = data1(1:160,:);
new1(161:320,:) = data2(1:160,:);
new1(321:480,:) = data3(1:160,:);

new2(1:160,:) = data1(161:320,:);
new2(161:320,:) = data2(161:320,:);
new2(321:480,:) = data3(161:320,:);

new3(1:160,:) = data1(321:480,:);
new3(161:320,:) = data2(321:480,:);
new3(321:480,:) = data3(321:480,:);

new4(1:160,:) = data1(481:640,:);
new4(161:320,:) = data2(481:640,:);
new4(321:480,:) = data3(481:640,:);

save('results/for_anova/cb_matrix_plot.mat', 'new1', 'new2', 'new3', 'new4');