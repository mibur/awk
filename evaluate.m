
clearvars ; close all; clc

% inizialize arrays
or_0 = zeros(160,5); or_180 = zeros(160,5); or_45 = zeros(160,5); or_45m = zeros(160,5); 
cb_0 = zeros(160,5); cb_180 = zeros(160,5); cb_45 = zeros(160,5); cb_45m = zeros(160,5);
% select feature
feat = "T_C";
%needs to be switched in line 24 as well

%real size of DIN-A5-image in mm
dinx = 210; diny = 148;
%real size of 1 ° viewing angle in mm
view = 10.5; % 10,48 


%%%%%%%%%%%%%%%%%%%%%%
%loop for shaping data
%%%%%%%%%%%%%%%%%%%%%%
cnt1 = 0; cnt2 = 0; cnt3 = 0; cnt4 = 0;
%iterate over subjects
for sub = 1:4
    subject = sprintf('results/results%d.mat', sub);
    load(subject);
    %iterate over feature maps
    for i = 1:numel(numbers)
        or = orientations(i);
        switch or %select rotation angle and set counter +1 (for each orientations different counter)
            case 1
                ang = 0;
                cnt1 = cnt1 + 1;
            case 2
                ang = -45;
                cnt2 = cnt2 + 1;
            case 3
                ang = -180;
                cnt3 = cnt3 + 1;
            case 4 
                ang = -225;
                cnt4 = cnt4 + 1;
        end
             
        nr = numbers(i);
        
        % feature maps as float matracies
        feature_file = sprintf('Merkmalskarten/T_C/%d.mat', nr);
        disp(feature_file)
        feature_temp = load(feature_file);
        if feat == "objMaps"
            featmap = feature_temp.objMap; 
        else
            featmap = feature_temp.featMap; end
        
        m_featmap = mean(featmap(:));
        std_featmap = std(featmap(:));
        
        sfm = size(featmap);
        for s1=1:sfm(1)
            for s2=1:sfm(2)
                featmap(s1,s2) = (featmap(s1,s2) - m_featmap) / std_featmap;
            end
        end
        
        % center of feature map
        [ny,nx] = size(featmap);
        Center = round([nx; ny]/2);        
        rot_vector = [cosd(ang) -sind(ang); sind(ang) cosd(ang)];
        
        % fläche pro pixel
        h = diny/ny;
        w = dinx/nx;
        
        %size of viewing window in pixel
        vw_x = view/w;
        vw_y = view/h;
        %radius of viewing window
        radius = round(((vw_x+vw_y)/2)/2);
        
        
        
        %iterate over klicks
        for k = 1:5
            x  = results(i,k);
            y  = results(i,k+5);
              
            %%%%%%%%%%%%%%%%%%%%%%%%%
            %calculate klick rotation
            %%%%%%%%%%%%%%%%%%%%%%%%%
            % reset coord to origin 
            coord = [x-Center(1);y-Center(2)];
            % multiply klick-coords with rotation vector, assuming the
            % coord-vector starts from (0,0)
            coord_rot = mtimes(rot_vector, coord);
            % add central shift to rotated coords
            coord_rot = round(coord_rot + Center);
            %feat_val = featmap(coord_rot(2),coord_rot(1));
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %calculate normalized feature value of viewing window
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            x0 = coord_rot(1);
            y0 = coord_rot(2);            
            [xgrid, ygrid] = meshgrid(1:nx, 1:ny);
            mask = ((xgrid-x0).^2 + (ygrid-y0).^2) <= radius.^2;
            values = featmap(mask);
            featval_norm = mean(values);
            
            
            %%%%%%%%%%%%%%%%%
            %central bias
            %%%%%%%%%%%%%%%%%
            %distance of klick coordinate to center
            dist = sqrt(coord_rot(2).^2 + coord_rot(1).^2);
            
            
            switch or % sort klick-coordinate to orientation array and central bias
                case 1                                    
                    or_0(cnt1, k) = featval_norm;
                    cb_0(cnt1, k) = dist;
                case 2
                    or_45m(cnt2, k) = featval_norm;
                    cb_45m(cnt2, k) = dist;
                case 3
                    or_180(cnt3, k) = featval_norm;
                    cb_180(cnt3, k) = dist;
                case 4
                    or_45(cnt4, k) = featval_norm;
                    cb_45(cnt4, k) = dist;
            end
        end
    end
end

%mibur: create matrix for anova
anova_matrix = vertcat(cb_0, cb_45, cb_180, cb_45m);
save('results/for_anova/cb_matrix.mat', 'anova_matrix');
save('results/for_anova/cb.mat', 'cb_0','cb_45','cb_180','cb_45m');
writematrix(anova_matrix,'results/for_anova/cb_matrix.txt','Delimiter','\t');

clearvars or sub i feature_temp subject x y k coord cnt1 cnt2 cnt3 cnt4 x0 y0 dist


